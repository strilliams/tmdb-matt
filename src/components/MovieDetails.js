import React from 'react'
import Moment from 'react-moment';
import '../styles/MovieDetails.scss'
import back from '../images/back.png'
import { Link } from '@reach/router'

class MovieDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activeMovie: []
        }
        this.apiKey = '6ed12e064b90ae1290fa326ce9e790ff'
    }

    componentDidMount() {
    // Load current movie
    fetch(`https://api.themoviedb.org/3${this.props.location.pathname}?api_key=${this.apiKey}`)
    .then(data => data.json())
    .then(data => {
      console.log (data)
      this.setState({ activeMovie: data})
    })
    }

    render() {
        const { activeMovie } = this.state
        return (
            <div className="movieDetails">
                <div className="backdropContainer">
                    <img className="backdrop" src={`http://image.tmdb.org/t/p/w500${activeMovie.backdrop_path}`} alt="Backdrop Image" />
                    <Link to="/" >
                    <img className="backButton" src={back} />
                    </Link>
                </div>
                

                <div className="contentArea">
                    <img className="posterMovie" src={`http://image.tmdb.org/t/p/w500${activeMovie.poster_path}`} alt="Movie Poster" />

                    <div className="movieInfo">
                        <h1>{activeMovie.title}</h1>

                        <Moment format="YYYY">
                        {activeMovie.release_date}
                        </Moment> ·
                        {activeMovie.vote_average * 10}% User Score
                        <br/>

                        {Math.floor(activeMovie.runtime/60)}h {activeMovie.runtime % 60} min
                    </div>
                </div>
                
                <div className="Overview">
                    <h2>Overview</h2>
                    <p>{activeMovie.overview}</p>
                </div>
            </div>
        )
    }
}

export default MovieDetails