import React, { Component } from 'react';
import logo from '../images/tmdb-logo.png'
import { Link } from '@reach/router'
import '../styles/Header.scss';

class Header extends Component {
    render() {
        return(
            <div className="header-container" >
                <Link to="/"><img className="logo" src={logo} alt="TMDB Logo" /></Link>
            </div>
        )
    }
}

export default Header;