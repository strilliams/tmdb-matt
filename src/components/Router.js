import React from 'react'
import { Router } from '@reach/router'

import App from '../App'
import MovieDetails from './MovieDetails'

const AppRouter = () => {
    return(
    <Router>
            <MovieDetails path="/movie/:id" />
            <App path="/" />
    </Router>
    )
}

export default AppRouter