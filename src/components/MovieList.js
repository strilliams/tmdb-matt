import React from 'react';
import '../styles/MovieList.scss';

import Movie from './Movie'

const MovieList = (props) => {
    return(
        <div className="pageContainer">
            <h2 className="movieListTitle">Popular Movies</h2>
            <div className="movieContainer">
            {props.movies.map((movie, i) =>
                    <Movie key={i} image={movie.poster_path} id={movie.id} rating={movie.vote_average} title={movie.title} date={movie.release_date} />
            )}
        </div>
        </div>
    )
}

export default MovieList;