import React from 'react';
import '../styles/SearchBar.scss';
import search from '../images/search.png'

const SearchBar = (props) => {
    return(
        <form onSubmit={props.handleSubmit}>
            <input className="searchBar" placeholder="Search" type="text" onChange={props.handleChange} />
            <img className="inputIcon" src={search} alt="Search icon" />
        </form>
    )
}

export default SearchBar;