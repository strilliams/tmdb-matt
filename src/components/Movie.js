import React from 'react';
import Moment from 'react-moment';
import '../styles/Movie.scss';
import { Link } from '@reach/router'

const Movie = (props) => {
    return(
        <div className="cardContainer">
            <Link to={`/movie/${props.id}`} >
                <img className="movieCard" src={`http://image.tmdb.org/t/p/w500${props.image}`} alt="Movie Poster" />
                <span className={"rating " + (props.rating > 7.5 ? 'green' : 'purple') + " " + (props.rating < 5 ? 'red' : null)}>{props.rating * 10}%</span>
            </Link>
            <p className="movieTitle">{props.title}</p>
            <p className="movieDate">
            <Moment format="MMMM YYYY">
                {props.date}
            </Moment>    
            </p>
            
        </div>
    )
}

export default Movie;