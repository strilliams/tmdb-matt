import React, { Component } from 'react'
import './styles/App.scss'

import Header from './components/Header.js'
import SearchBar from './components/SearchBar.js'
import MovieList from './components/MovieList.js'


class App extends Component {
  constructor() {
    super()
    this.state = {
      movies: [],
      searchTerm: ''
    }
  this.apiKey = '6ed12e064b90ae1290fa326ce9e790ff'
  }

  componentDidMount() {
    // Load popular movies
    fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${this.apiKey}`)
    .then(data => data.json())
    .then(data => {
      console.log (data)
      this.setState({ movies: [...data.results]})
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&query=${this.state.searchTerm}`)
    .then(data => data.json())
    .then(data => {
      console.log (data)
      this.setState({ movies: [...data.results]})
    })
  }

  handleChange = (e) => {
    this.setState({ searchTerm: e.target.value })
  }

  render() {

    return (
      <div className="App">
          <Header />
          <SearchBar handleSubmit={this.handleSubmit} handleChange={this.handleChange} />

          <MovieList movies={this.state.movies} />

      </div>
    );
  }
}

export default App;
